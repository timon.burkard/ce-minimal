# CEminimal

Copyright (C) 2023 Timon Burkard

CEminimal is a minimalistic chess engine

## Current limitations
 - Only pawn moves are supported, i.e., the other pieces cannot yet move
 - En passant is not implemented
 - Pawn promotion is not implemented
 - Performance optimizations are pending
