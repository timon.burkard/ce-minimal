##########################################################
# imports
##########################################################

from enum import Enum
from typing import Tuple
import copy

##########################################################
# types
##########################################################

class Players(Enum):
    WHITE = 1
    BLACK = 2

state_t = list[list[str]]

##########################################################
# functions
##########################################################

"""Chess engine based on Minimax algorithm

Args:
    state (state_t):  game state
    depth (int):      minimax algorithm depth
    player (Players): player whos turn it is

Returns:
    value (int):     max/min value calculated for the current game state
    state (state_t): suggested next game state
"""
def minimax(state: state_t, depth: int, player: Players) -> Tuple[int, state_t]:
    if is_terminal(state) or (depth == 0):
        return get_value(state), copy.deepcopy(state)

    if player == Players.WHITE:
        # WHITE players turn -> tries to maximize the value
        possible_actions = get_possible_actions(state, Players.WHITE)
        possible_states = [execute_action(state, action) for action in possible_actions]

        if len(possible_states) == 0:
            # no possible moves --> draw by stalemate
            return 0, copy.deepcopy(state)

        value = -float('inf')
        for action in possible_actions:
            tmp, _ = minimax(execute_action(state, action), depth - 1, Players.BLACK)
            if tmp > value:
                value = tmp
                return_state = execute_action(state, action)

        return value, return_state

    else:
        # BLACK players turn -> tries to minimize the value
        possible_actions = get_possible_actions(state, Players.BLACK)
        possible_states = [execute_action(state, action) for action in possible_actions]

        if len(possible_states) == 0:
            # no possible moves --> draw by stalemate
            return 0, copy.deepcopy(state)

        value = float('inf')
        for action in possible_actions:
            tmp, _ = minimax(execute_action(state, action), depth - 1, Players.WHITE)
            if tmp < value:
                value = tmp
                return_state = execute_action(state, action)

        return value, return_state

"""Wheter or not a game state is a terminal state

Args:
    state (state_t): game state

Returns:
    bool: is game state terminal
"""
def is_terminal(state: state_t) -> bool:
    if any('K' in row for row in state):
        return False

    if any('k' in row for row in state):
        return False

    return True

"""get estimated value of a game state

Args:
    state (state_t): game state

Returns:
    int: estimated value of the game state
"""
def get_value(state: state_t) -> int:
    value = 0

    value += sum(row.count('P') for row in state) * PIECE_VALUES['P']
    value += sum(row.count('N') for row in state) * PIECE_VALUES['N']
    value += sum(row.count('B') for row in state) * PIECE_VALUES['B']
    value += sum(row.count('R') for row in state) * PIECE_VALUES['R']
    value += sum(row.count('Q') for row in state) * PIECE_VALUES['Q']
    value += sum(row.count('K') for row in state) * PIECE_VALUES['K']

    value -= sum(row.count('p') for row in state) * PIECE_VALUES['p']
    value -= sum(row.count('n') for row in state) * PIECE_VALUES['n']
    value -= sum(row.count('b') for row in state) * PIECE_VALUES['b']
    value -= sum(row.count('r') for row in state) * PIECE_VALUES['r']
    value -= sum(row.count('q') for row in state) * PIECE_VALUES['q']
    value -= sum(row.count('k') for row in state) * PIECE_VALUES['k']

    return value

"""Create a move for a piece

    Piece should move from (row_from, col_from) to (row_to, col_to).

    The new position of the piece is marked by the piece symbol. The previous position of the piece as marked by an 'X'.

    Args:
        piece (str[1]): piece
        row_from (int): row from where the piece is moving
        col_from (int): col from where the piece is moving
        row_to (int):   row to where the piece is moving
        col_to (int):   col to where the piece is moving

    Returns:
        state_t: move
    """
def create_move(piece: str, row_from: int, col_from: int, row_to: int, col_to: int) -> state_t:
    field = copy.deepcopy(EMPTY_FIELD)

    field[row_from][col_from] = 'X'
    field[row_to][col_to]     = piece

    return field

"""Get all possible moves for a given piece in a given game state

    Args:
        state (state_t): game state
        piece (str[1]):  piece
        row (int):       row of the piece
        col (int):       col of the piece

    Returns:
        list(state_t): moves
    """
def get_possible_moves(state: state_t, piece: str, row: int, col: int) -> list[state_t]:
    moves = list()

    match piece:
        case 'P':
            if (row != 0) and (state[row-1][col] == ' '):
                # pawn can move up one field
                moves.append(create_move(piece, row, col, row-1, col))

            if (row == 6) and (state[row-1][col] == ' ') and (state[row-2][col] == ' '):
                # pawn is at starting position and can move up two fields
                moves.append(create_move(piece, row, col, row-2, col))

            if (row != 0) and (col != 7) and (state[row-1][col+1] in BLACK_PIECES):
                # pawn can take a black piece on its top right side
                moves.append(create_move(piece, row, col, row-1, col+1))

            if (row != 0) and (col != 0) and (state[row-1][col-1] in BLACK_PIECES):
                # pawn can take a black piece on its top left side
                moves.append(create_move(piece, row, col, row-1, col-1))

        case 'N':
            pass #TODO

        case 'B':
            pass #TODO

        case 'E':
            pass #TODO

        case 'Q':
            pass #TODO

        case 'K':
            pass #TODO

        case 'p':
            if (row != 7) and (state[row+1][col] == ' '):
                # pawn can move up one field
                moves.append(create_move(piece, row, col, row+1, col))

            if (row == 1) and (state[row+1][col] == ' ') and (state[row+2][col] == ' '):
                # pawn is at starting position and can move up two fields
                moves.append(create_move(piece, row, col, row+2, col))

            if (row != 7) and (col != 0) and (state[row+1][col-1] in WHITE_PIECES):
                # pawn can take a white piece on its top right side
                moves.append(create_move(piece, row, col, row+1, col-1))

            if (row != 7) and (col != 7) and (state[row+1][col+1] in WHITE_PIECES):
                # pawn can take a white piece on its top left side
                moves.append(create_move(piece, row, col, row+1, col+1))

        case 'n':
            pass #TODO

        case 'b':
            pass #TODO

        case 'r':
            pass #TODO

        case 'q':
            pass #TODO

        case 'k':
            pass #TODO

    return moves

"""Get all possible actions in a given game state

    Args:
        state (state_t):  game state
        player (Players): current player

    Returns:
        list(state_t): actions
    """
def get_possible_actions(state: state_t, player: Players) -> list[state_t]:
    actions = list()

    if player == Players.WHITE:
        for row in range(0, 8):
            for col in range(0, 8):
                if state[row][col] in WHITE_PIECES:
                    # found a piece to check
                    actions.extend(get_possible_moves(state, state[row][col], row, col))

    else:
        for row in range(0, 8):
            for col in range(0, 8):
                if state[row][col] in BLACK_PIECES:
                    # found a piece to check
                    actions.extend(get_possible_moves(state, state[row][col], row, col))

    return actions

"""Execute an action

    Args:
        state (state_t):  game state
        action (state_t): action

    Returns:
        state_t: Modified game state
    """
def execute_action(state: state_t, action: state_t) -> state_t:
    modified_state = copy.deepcopy(state)

    for row in range(0, 8):
        for col in range(0, 8):
            if action[row][col] == 'X':
                # piece has to be removed from this field
                modified_state[row][col] = ' '
            elif action[row][col] != ' ':
                # piece has to be added to this field
                modified_state[row][col] = action[row][col]

    return modified_state

"""Print a game state as field

    Args:
        state (state_t): game state
    """
def print_field(state: state_t) -> None:
    for row in state:
        row_string = ' '.join(list(map(lambda x: x.replace(' ', '.'), row)))
        print(f"{row_string}")

##########################################################
# constants
##########################################################

# Whites pieces are upper case
# Blacks pieces are lower case
#
# R/r: Rock
# N/n: Knight
# B/b: Bishop
# Q/q: Queen
# K/k: King
# P/p: Pawn
WHITE_PIECES = "PNBRQK"
BLACK_PIECES = "pnbrqk"

PIECE_VALUES = {
                   'P': 1,
                   'p': 1,
                   'N': 3,
                   'n': 3,
                   'B': 3,
                   'b': 3,
                   'R': 5,
                   'r': 5,
                   'Q': 9,
                   'q': 9,
                   'K': 1000,
                   'k': 1000,
               }

# (0,0) (0,1) (0,2) (0,3) (0,4) (0,6) (0,6) (0,7)
# (1,0) (1,1) (1,2) (1,3) (1,4) (1,6) (1,6) (1,7)
# (2,0) (2,1) (2,2) (2,3) (2,4) (2,6) (2,6) (2,7)
# (3,0) (3,1) (3,2) (3,3) (3,4) (3,6) (3,6) (3,7)
# (4,0) (4,1) (4,2) (4,3) (4,4) (4,6) (4,6) (4,7)
# (5,0) (5,1) (5,2) (5,3) (5,4) (5,6) (5,6) (5,7)
# (6,0) (6,1) (6,2) (6,3) (6,4) (6,6) (6,6) (6,7)
# (7,0) (7,1) (7,2) (7,3) (7,4) (7,6) (7,6) (7,7)
START_FIELD = [
                  ['r','n','b','q','k','b','n','r'],
                  ['p','p','p','p','p','p','p','p'],
                  [' ',' ',' ',' ',' ',' ',' ',' '],
                  [' ',' ',' ',' ',' ',' ',' ',' '],
                  [' ',' ',' ',' ',' ',' ',' ',' '],
                  [' ',' ',' ',' ',' ',' ',' ',' '],
                  ['P','P','P','P','P','P','P','P'],
                  ['R','N','B','Q','K','B','N','R'],
              ]

EMPTY_FIELD = [
                  [' ',' ',' ',' ',' ',' ',' ',' '],
                  [' ',' ',' ',' ',' ',' ',' ',' '],
                  [' ',' ',' ',' ',' ',' ',' ',' '],
                  [' ',' ',' ',' ',' ',' ',' ',' '],
                  [' ',' ',' ',' ',' ',' ',' ',' '],
                  [' ',' ',' ',' ',' ',' ',' ',' '],
                  [' ',' ',' ',' ',' ',' ',' ',' '],
                  [' ',' ',' ',' ',' ',' ',' ',' '],
              ]

##########################################################
# script
##########################################################

state = copy.deepcopy(START_FIELD)

print_field(state)

player = Players.WHITE

while True:

    value, state = minimax(state, 2, player)

    print(f"Best choice for {player.name} is (value = {value}):")
    print_field(state)

    # next players move
    if player == Players.WHITE:
        player = Players.BLACK
    else:
        player = Players.WHITE
